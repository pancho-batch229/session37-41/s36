const User = require("../models/Users.js");
const bcrypt = require("bcrypt");
const authenticate = require("../auth.js");

// Check if the email already exists

	/*
		Steps: 
		1. Use mongoose "find" method to find duplicate emails
		2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
	*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		// conditional statement
		if(result.length > 0) {
			return true;
		} else {
			return false;
		}
	});
}

// User registration
	/*
		Steps:
		1. Create a new User object using the mongoose model and the information from the request body
		2. Make sure that the password is encrypted
		3. Save the new User to the database
	*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// 10 is the value provided as the number of "salt"
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	// Saves the created object to our database

	return newUser.save().then((user, error) => {
		// if registration failed
		if(error){
			return false;
		} else {
			// if registration is successful
			return true;
		}
	});
}

// User authentication
	/*
		Steps:
		1. Check the database if the user email exists
		2. Compare the password provided in the login form with the password stored in the database
		3. Generate/return a JSON web token if the user is successfully logged in and return false if not
	*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: authenticate.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
}