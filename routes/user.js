const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")

// Check email if it is existing to our database
router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// User Registration route
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User Login
router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// makes this file accessible 
module.exports = router;